﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCntr : MonoBehaviour {

	public float speed;
    public Text countText;
    public Text winText;

	private Rigidbody rb;
	private int count;

	// Use this for initialization
	void Start () {
		
		rb = GetComponent<Rigidbody> ();
		count = 0;
        SetCountText();
        winText.text = "";
     
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
   
		Vector3 movement = new Vector3 (horizontal, 0.0f, vertical);

		rb.AddForce(movement * speed);
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup"))
        {
			other.gameObject.SetActive (false);
			count = count + 1;
            SetCountText();
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if(count>=20)
        {
            winText.text = "You Win!";
        }
    }
}
